login2$ cat README
Initial code create on 9/3/2013.
name:
email: 

login2$ cat prog.f90
program invsqrt
integer, parameter :: N=100
real a(N);
   do i=1,N; a(i)=i; end do
   call isqrt(a,N);
   print*,"N a(1) a(N): ", N,a(1),a(N)
end program

login2$ cat routines.f90
subroutine isqrt(a, n)
  integer :: n,i
  real    :: a(n)
!                Vector inverse square
!                Someone advised to 
!                making two separate loops.

// combined loops together
  do i=1,n; a(i) = sqrt(a(i)); a(i) = 1.0e0/a(i);  end do

end subroutine


#include <stdio.h>
#define  Nlist    7
#include "parameters.h" // if i dont include parameters, the output doesn't come to expect. why?
// #include "routines.c"  //including routines.c causes multiple function problem. so does it include the function twice in the architec?

int main(){

double x_start, x_end;
double h, xi;

double sum_midp,  sum_trap,  sum_simp;
double  int_midp, int_trap, int_simp;


int i,l,m;

int list[Nlist]={ 11, 101, 1001, 10001, 100001,  1000001, 10000001};


printf("M\t\t  Mid Point\t\t Trapezoid\t\t Sampson\t\t  Midpoint Error\t\t Trapezoid Error\t\t Sampson Error\t\t  Actual\n");


x_start = 0.0e0;
x_end   = 1.0e0;

for(l=0;l<Nlist;l++){

   m = list[l];
   h=(x_end-x_start)/(m-1);

   sum_midp = 0.0e0;
   sum_trap = 0.0e0;
   sum_simp = 0.0e0;

   for(i=0;i<m-1;i++){
      xi   = (x_start + i*h);
      sum_midp = sum_midp + midp(xi,h);
      sum_trap = sum_trap + trap(xi,h);
   }

 
   for(i=1;i<m-1;i+=2){
      xi   = (x_start + i*h);
      sum_simp = sum_simp + simp(xi,h);
   }
   int_midp = h*sum_midp;
   int_trap = h*sum_trap/2;
   int_simp = h*sum_trap/3; 

   printf(" %12i %21.13f %21.13f %21.13f %21.13f %21.13f %21.13f 0.125\n",  m, int_midp, int_trap,int_simp, .125 - int_midp, .125 - int_trap, .125 -  int_simp);

 }
}

#include <math.h>

double midp(double xi, double h){
double  x;
   x = pow( (xi + h*0.5e0),(double)7 );
   return(x);
}

double trap(double xi, double h){
double  x;
   x= pow( xi,(double)7 ) + pow( (xi + h),(double)7 );
   return(x);
}

double simp(double xi, double h){
double  x;
  x=pow( (xi - h),(double)7 ) + 4 * pow( xi,(double)7 )+ pow( (xi + h),(double)7 );
  return(x);
}
